package com.company;

public class MakeUp implements Pesan,Review{
    public String namaproduk;
    public Integer pengguna;

    public void setNamaproduk(String namaproduk) {
        this.namaproduk = namaproduk;
    }

    @Override
    public void produk() {
        System.out.println("Nama Produk             : "+namaproduk);
    }

    @Override
    public void total() {

    }

    @Override
    public void detail() {
        System.out.println("Disarankan sebelum penggunaan makeup menggunakan toner untuk melembabkan dan jangan sering menggunakan makeup berlebih");
    }
}

package com.company;

public class Perawatan_Muka implements Pesan,Review {
    public String namaproduk;

    public void setNamaproduk(String namaproduk) {
        this.namaproduk = namaproduk;
    }

    @Override
    public void produk() {
        System.out.println("Nama Produk             : "+namaproduk);
    }

    @Override
    public void total() {

    }

    @Override
    public void detail() {
        System.out.println("Untuk kulit muka sensitif sangat cocok menggunakan produk ini karena tidak mengantung alkohol dan paraben");
    }
}

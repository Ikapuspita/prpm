package com.company;

public class Produk implements Pesan,Review {
    public Integer harga;
    public Integer jumlah;
    public String namaproduk;
    private final Integer saldo=100000;

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public void setNamaproduk(String namaproduk) {
        this.namaproduk = namaproduk;
    }

    @Override
    public void produk() {
        System.out.println("Nama Produk : "+namaproduk);
    }

    @Override
    public void total() {
        System.out.println("Total Harga : "+jumlah*harga);
    }

    public void saldo(){
        System.out.println("Saldo Anda  : "+saldo);
    }

    @Override
    public void detail() {
        System.out.println("Review      : Untuk kulit sensitif sangat bagus karna tidak bikin breakout");
    }
}
